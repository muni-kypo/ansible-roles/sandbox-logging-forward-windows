# Ansible role - KYPO Sandbox Logging Forwarding Windows
This role provides forwarding of the local logs from sandbox host running on Windows to the specified remote host.

[[_TOC_]]

## Description
This role supports hosts running on Windows (tested on `munikypo/windows-server-2019` image).
It supports forwarding of logs from Eventlog and also logs from specified log files.

## Requirements
* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```
  
## Parameters
### Mandatory parameters
- `slf_win_cls_destination_ip` - IP address of the log destination host

### Optional parameters
- `slf_win_cls_destination_port` - Remote host destination port (default `514`)  
- `slf_win_cls_destination_protocol` - Transport protocol (default `tcp`, available also `udp`)  
- `slf_win_nxlog_root_dir` - Path to NXLog installation (default `C:/Program Files (x86)/nxlog`)  
- `slf_win_log_files` - List of files that should be treated as logs and transmitted to log destination host. Example:  
  ```
    slf_win_log_files:
      - path: "C:/Users/Admin/logfile.log"
      - path: "C:/Users/windows/AppData/Roaming/Microsoft/Windows/PowerShell/PSReadLine/ConsoleHost_history.txt"
  ```

### Example

The simplest example of sandbox logging forward configuration.

```yaml
roles:
  - role: sandbox-logging-forward-windows
    slf_win_cls_destination_ip: 10.10.0.12
    become: yes
```

